#pragma once
#ifndef WEATHER_H
#define WEATHER_H

#include "constants.h"
#include <string>

struct date
{
    unsigned int day;
    unsigned int month;
};

struct amount
{
    float amount;
};
struct characteristic 
{    
    std::string characteristic;
};
struct weather
{
    date day;
    date month;
    amount amount;

    char title[MAX_STRING_SIZE];
};

#endif
